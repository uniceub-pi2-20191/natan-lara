import React, {Component} from 'react';
import {Text, View, StyleSheet, ScrollView, Image} from 'react-native';


export default class App extends Component{
  render() {
    return (
		<ScrollView>
    		<View style={styles.container}>
		
			<View style={styles.blocoTexto}>
				<Text style={styles.textos}>1. Hamburguer</Text>
				<Text style={styles.textos}>2. Batata Frita</Text>
				<Text style={styles.textos}>3. Refrigerante</Text>
			</View>

			<View style={styles.blocoImagem}>
				<Image 
				style={styles.imagens}
				source={{uri: 'https://s2.glbimg.com/S8SxT4O8PBbd19MNJL0h7ttwnMg=/smart/e.glbimg.com/og/ed/f/original/2018/05/25/colunas-vamos-receber-peraltex-burger-03.jpg'}}
				/>

				<Image 
				style={styles.imagens}
				source={{uri: 'https://www.radioclubejoinville.com.br/wp-content/uploads/sites/455/2018/08/batata-frita-4.png'}}
				/>

				<Image 
				style={styles.imagens}
				source={{uri: 'https://s2.glbimg.com/GUda5oj9xkd_yQNyn36mDn9XJmo=/620x455/e.glbimg.com/og/ed/f/original/2018/08/17/beber-refrigerante-todos-os-dias-esta-te-matando.jpg'}}
				/>
			</View>

    		</View>
	  	</ScrollView>
    );
  }
}

const styles = StyleSheet.create({

  container: {
	paddingTop: 75,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },

  textos: {
    fontSize: 25,
    fontWeight: 'bold',
    color: 'blue',
  },

  imagens: {
	  marginBottom: 20,
	  width: 400,
	  height: 300,
  },

  blocoTexto: {
	paddingTop: 50,
	paddingBottom: 50,
  },

  blocoImagem: {
	paddingTop: 50,
	
  }

});